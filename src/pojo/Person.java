package pojo;

public class Person {
    private Integer pid;
    private String name;
    private Integer age;

    public Person() {
    }

    public Person(Integer pid, String name, Integer age) {
        this.pid = pid;
        this.name = name;
        this.age = age;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "pid=" + pid +
                ", name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
