package Utils;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

//获取sqlsession方法
public class Utils {
    private static SqlSessionFactory sqlSessionFactory;
    private static Object AutoCloseable;

    static {
    //加载核心配置文件
    InputStream resourceAsStream = null;
    try {
        resourceAsStream = Resources.getResourceAsStream("Sqlconfig.xml");
         sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsStream);
    } catch (IOException e) {
        e.printStackTrace();
    }

}



    public static SqlSession getSession() throws IOException {

        SqlSession sqlSession = sqlSessionFactory.openSession((Boolean) AutoCloseable);
        return sqlSession;
    }
}
