package dao;

import Utils.Utils;
import org.apache.ibatis.session.SqlSession;
import pojo.Card;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.List;

public class Test {
    @org.junit.Test
    public void findall() throws IOException {
        //获得sql对象
        SqlSession session = Utils.getSession();
        //通过sqlsession获得Dao对象
        Dao mapper = session.getMapper(Dao.class);
        List<Card> list = mapper.findAll1();
        for (Card card : list) {
            System.out.println(card);
        }
        session.close();

    }
}
